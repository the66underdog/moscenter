import exp from 'express';
import fs from 'fs';
import path from 'path';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

let app = exp();

app.use(exp.json());

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    next();
});

app.use((req, res, next) => {
    req.srcFolder = 'vacancies/';
    req.src = 'example_1.xml';
    req.filePath = path.join(__dirname);
    next();
});

app.get('/vacancies', (req, res) => {
    fs.readFile(path.join(req.filePath, req.srcFolder, req.src), (err, data) => {
        if (err) next(err);
        else {
            res.send(data);
        }
    });
});

app.use(errorHandler);

function errorHandler(err, req, res, next) {
    res.writeHead(500);
    res.write(err);
    res.end();
}

app.listen(8000, (err) => {
    err ? console.log('Error:' + err) : console.log(`It' fine`);
});