import '../styles/style.css';
import {
    listVacancies,
} from './selectors.js';
import './scroller.js';
import { src } from './getXMLdata.js';
import generateVacancy from './generateVacancy.js';
import showDescription from './showDescription.js';
import formPagination from './formPagination.js';

const vacAmount = src.getElementsByTagName('vacancy').length;
const listSize = 5;

export default function formListVacancies(currentPage = 0) {
    const amount = (vacAmount - currentPage * listSize) < 5 ?
        vacAmount - currentPage * listSize : 5;
    listVacancies.replaceChildren();
    for (let i = currentPage * listSize; i < currentPage * listSize + amount; i++) {
        listVacancies.append(generateVacancy(i));
    }
    listVacancies.addEventListener('click', showDescription);
}

formListVacancies();
formPagination();
