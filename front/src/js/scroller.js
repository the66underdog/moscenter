import {
    sliderList as slider,
    sliderBtn as controllers,
} from "./selectors.js";

let currentSlide = 0;
const nums = slider.children.length;

function scrollSlider(dir = 1) {
    if (dir > 0) {
        currentSlide = currentSlide < nums - 1 ? ++currentSlide : 0;
    }
    else {
        currentSlide = currentSlide > 0 ? --currentSlide : nums - 1;
    }
    slider.scrollTo(currentSlide * slider.clientWidth, 0);
}

controllers[0].addEventListener('click', () => scrollSlider(-1));
controllers[1].addEventListener('click', () => scrollSlider());
window.addEventListener('resize', function () {
    slider.scrollTo({
        left: currentSlide * slider.clientWidth,
        behavior: 'instant'
    });
});