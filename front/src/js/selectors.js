export const listVacancies = document.getElementsByClassName('vacancies__list')[0];
export const description = document.getElementsByClassName('description')[0];

export const sliderList = document.getElementsByClassName('slider__list')[0];
export const sliderBtn = document.getElementsByClassName('slider__btn');

export const pagination = document.getElementsByClassName('pagination')[0];