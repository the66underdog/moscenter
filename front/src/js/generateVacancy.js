import { jobNames, salaries, currencies } from "./getXMLdata";

export default function generateVacancy(id) {
    const vacancy = document.createElement('li');
    const container = document.createElement('div');
    const name = document.createElement('h3');
    const salary = document.createElement('h4');
    const button = document.createElement('button');

    name.innerText = jobNames[id].textContent;
    button.innerText = 'Подробнее';
    salary.innerText = `${salaries[id].textContent} ${currencies[id].textContent}`;

    button.dataset.id = id;

    vacancy.classList.add('block', 'vacancy', 'vacancies__item');
    container.classList.add('vacancy__container');
    name.classList.add('vacancy__header');
    salary.classList.add('vacancy__salary');
    button.classList.add('btn', 'vacancy__btn');

    container.append(name);
    container.append(salary);
    vacancy.append(container);
    vacancy.append(button);
    return vacancy;
}