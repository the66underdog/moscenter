import { currencies, desc, jobNames, requirement, salaries, schedule } from "./getXMLdata";
import { description } from "./selectors";

export default function showDescription(e) {
    const btnContext = e.target.closest('.vacancy__btn');
    if (btnContext) {
        const vacID = btnContext.dataset.id;
        const descriptionContent = description.firstElementChild;

        descriptionContent.children[0].innerText = jobNames[vacID].childNodes[0].data;
        descriptionContent.children[1].innerHTML = `<b>${salaries[vacID].childNodes[0].data} ${currencies[vacID].childNodes[0].data}</b>`;
        descriptionContent.children[2].innerHTML = desc[vacID].childNodes[0].data;
        descriptionContent.children[3].firstElementChild.innerHTML = `<b>График</b>: ${schedule[vacID].textContent} | <b>Образование</b>: ${requirement[vacID].children[0].textContent} | <b>Опыт</b>: ${requirement[vacID].children[1].textContent}`;

        description.classList.add('description_active');
    }

    window.addEventListener('keydown', hideDescription);
    window.addEventListener('mouseup', hideDescription);
}

function hideDescription(e) {
    if (e.type === 'mouseup') {
        if (!e.target.closest('.description__content')) {
            description.classList.remove('description_active');
        }
    }
    if (e.type === 'keydown') {
        if (e.key === 'Escape') {
            description.classList.remove('description_active');
        }
    }
}