import formListVacancies from ".";
import { src } from "./getXMLdata";
import { pagination } from "./selectors";

const vacAmount = src.getElementsByTagName('vacancy').length;
const listSize = 5;
const pagesAmount = Math.ceil(vacAmount / listSize);

export default function formPagination() {
    for (let i = 0; i < pagesAmount; i++) {
        let paginationItem = document.createElement('li');
        let paginationBtn = document.createElement('button');

        paginationBtn.innerText = `${i + 1}`;

        paginationItem.classList.add('pagination__item');
        paginationBtn.classList.add('btn', 'pagination__btn');
        paginationBtn.dataset.page = i;

        paginationItem.append(paginationBtn);
        pagination.append(paginationItem);
    }
    pagination.addEventListener('click', changePage);
}

function changePage(e) {
    const btnContext = e.target.closest('.pagination__btn');
    if (btnContext) {
        formListVacancies(+btnContext.dataset.page);
    }
}