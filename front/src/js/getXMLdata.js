async function getXMLdata() {
    const res = await fetch('http://localhost:8000/vacancies');
    if (res.ok) {
        const xmlStr = await res.text();
        const xmlParser = new DOMParser();
        return xmlParser.parseFromString(xmlStr, 'text/xml');
    }
}

export const src = await getXMLdata();

export const jobNames = src.getElementsByTagName('job-name');
export const salaries = src.getElementsByTagName('salary');
export const currencies = src.getElementsByTagName('currency');

export const desc = src.getElementsByTagName('description');
export const schedule = src.getElementsByTagName('schedule');
export const requirement = src.getElementsByTagName('requirement');